fbpanel-genmon2
---------------

slightly modified genmon plugin (generic monitor = display command output) for 
fbpanel.  
based on this: https://github.com/naihe2010/fbpanel

This is my first C-compiled project - it compiles nicely on my system, and
installs only libgenmon2.so to /usr/lib/fbpanel, but there's no guarantee it
will do the same on yours.

#### Differences from the original genmon plugin:  
I removed the insanely useless default color (if you don't manually specify a 
color, the original genmon plugin defaults to darkblue, instead of just using 
what gtk offers) and default font, and increased maximum characters from 30 to
99.
So instead of trying to look good, genmon2 just takes current GTK theme colors,
unless the user specifies something else.  
TODO: I would like to add multiple lines output, if it can be done without 
modifying fbpanel itself.

It's up to the user to add markup tags around their output, that GTK 
understands. I believe it is pango markup, as described [here][1].  
I also find that the plugin does not like '&' and similar characters; please 
have a look at the example script mocp_fbpanel, where I am using a Perl
module to encode these entities.

With this "reduced" functionality, genmon2 only takes very few options:

	# snippet from ~/.config/fbpanel/default:
	Plugin {
		type = genmon2
		config {
		Command = ~/.config/fbpanel/scripts/mocp_fbpanel
		PollingTime = 2
		}
	}

...unless there are even more undocumented features.  
fbpanel hasn't ceased to surprise me yet.

[1]: https://developer.gnome.org/pango/stable/PangoMarkupFormat.html